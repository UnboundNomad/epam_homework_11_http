package Demo;

import com.mashape.unirest.http.exceptions.UnirestException;
import http.ApacheHttp;
import http.JavaHttp;
import http.UnirestHttp;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Demo {

    public void execute() {

        String url = "http://httpbin.org/";

        Map<String, String> headers = new HashMap<String, String>() {
            {
                put("accept", "application/json");
            }
        };

        Map<String, String> parameters = new HashMap<String, String>() {
            {
                put("param1", "cats");
                put("param2", "dogs");
            }
        };

        try {
            title ("HttpURLConnection", "GET", url + "get");
            new JavaHttp().executeGet(url + "get", headers, parameters);
            line();

            title ("HttpURLConnection", "POST", url + "post");
            new JavaHttp().executePost(url + "post", headers, parameters);
            line();

            title ("ApacheHttp", "GET", url + "get");
            new ApacheHttp().executeGet(url + "get", headers, parameters);
            line();

            title ("ApacheHttp", "POST", url + "post");
            new ApacheHttp().executePost(url + "post", headers, parameters);
            line();

            title ("UnirestHttp", "GET", url + "get");
            new UnirestHttp().executeGet(url + "get", headers, parameters);
            line();

            title ("UnirestHttp", "POST", url + "post");
            new UnirestHttp().executePost(url + "post", headers, parameters);
        } catch (IOException | UnirestException e) {
            e.printStackTrace();
        }
    }

    private void title(String header, String method, String url) {
        System.out.printf("\t%s, %s, %s:\n\n", header, method, url);
    }

    private void line() {
        System.out.println("----------------------------------------------");
    }

}
