package http;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ApacheHttp {

    public void executeGet(String urlAddress, Map<String, String> headers, Map<String, String> parameters) throws IOException {

        //Добавляем парметры
        StringBuilder getData = new StringBuilder(urlAddress);
        getData.append("?");
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            getData.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        getData.deleteCharAt(getData.length() - 1);

        HttpGet request = null;
        try {
            //Создаем клиент и делаем запрос
            HttpClient client = HttpClientBuilder.create().build();
            request = new HttpGet(getData.toString());

            //Указываем заголовки
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                request.addHeader(entry.getKey(), entry.getValue());
            }

            //Выводим результат
            output(client.execute(request));

        } finally {
            if (request != null) {
                request.releaseConnection();
            }
        }
    }

    public void executePost(String urlAddress, Map<String, String> headers, Map<String, String> parameters) throws IOException {

        HttpPost request = null;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            request = new HttpPost(urlAddress);

            //Указываем заголовки
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                request.addHeader(entry.getKey(), entry.getValue());
            }

            //Добавляем параметры
            List<NameValuePair> urlParameters = new ArrayList<>();
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
            request.setEntity(new UrlEncodedFormEntity(urlParameters));

            //Выводим результат
            output(client.execute(request));

        } finally {
            if (request != null) {
                request.releaseConnection();
            }
        }

    }

    private void output(HttpResponse response) throws IOException {

        HttpEntity entity = response.getEntity();

        //Выводим заголовки
        System.out.printf("Status line: %s\n", response.getStatusLine().toString());
        for (Header header : response.getAllHeaders()) {
            System.out.printf("%s: %s\n", header.getName(), header.getValue());
        }

        //Выводим тело
        if (entity != null) {
            String data = IOUtils.toString(entity.getContent(), "cp1251");
            System.out.println("\nBody: " + data);
        }
    }
}
