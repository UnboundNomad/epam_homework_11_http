package http;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.HashMap;
import java.util.Map;

public class UnirestHttp {

    public void executeGet(String urlAddress, Map<String, String> headers, Map<String, String> parameters) throws UnirestException {

        Map<String, Object> objectParameters = new HashMap<>(parameters);

        HttpResponse<String> response = Unirest.get(urlAddress)
                .headers(headers)
                .queryString(objectParameters)
                .asString();

        output(response);

    }

    public void executePost(String urlAddress, Map<String, String> headers, Map<String, String> parameters) throws UnirestException {

        Map<String, Object> objectParameters = new HashMap<>(parameters);

        HttpResponse<String> response = Unirest.post(urlAddress)
                .headers(headers)
                .fields(objectParameters)
                .asString();

        output(response);
    }

    private void output(HttpResponse<?> response) {

        System.out.printf("Status: %s, %s\n", response.getStatus(), response.getStatusText());
        System.out.println("Headers:\n" + response.getHeaders());
        System.out.println("Body: " + response.getBody());
    }

}