package http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class JavaHttp {

    private HttpURLConnection httpConnection;

    public void executeGet(String urlAddress, Map<String, String> headers, Map<String, String> parameters) throws IOException {

        //Добавляем парметры
        StringBuilder getData = new StringBuilder(urlAddress);
        getData.append("?");
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            getData.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        getData.deleteCharAt(getData.length() - 1);

        //Создаём URL
        URL url = new URL(getData.toString());

        try {
            //Создаём connection с помощью URL
            httpConnection = (HttpURLConnection) url.openConnection();

            //Указываем метод
            httpConnection.setRequestMethod("GET");

            //Указываем заголовки
            setHeaders(headers);

            //Выводим ответ
            output();
        } finally {
            httpConnection.disconnect();
        }
    }

    public void executePost(String urlAddress, Map<String, String> headers, Map<String, String> parameters) throws IOException {

        //Создаём массив с параметрами
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(entry.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes(StandardCharsets.UTF_8);

        //Создаём URL
        URL url = new URL(urlAddress);

        try {
            //Создаём connection с помощью URL
            httpConnection = (HttpURLConnection) url.openConnection();

            //Указываем метод
            httpConnection.setRequestMethod("POST");

            //Хотим отправлять данные
            httpConnection.setDoOutput(true);

            //Указываем заголовки
            setHeaders(headers);

            //Отправляем параметры
            httpConnection.getOutputStream().write(postDataBytes);

            //Выводим ответ
            output();
        } finally {
            httpConnection.disconnect();
        }
    }

    private void setHeaders(Map<String, String> headers) {
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            httpConnection.setRequestProperty(entry.getKey(), entry.getValue());
        }
    }

    private void output() throws IOException {

        //Выводим заголовки
        Map<String, List<String>> headerFields = httpConnection.getHeaderFields();
        Set<String> keySet = headerFields.keySet();
        System.out.println("Headers:");
        for (String key : keySet) {
            System.out.printf("%s: %s\n", key, headerFields.get(key));
        }

        //Выводим тело
        String line;
        StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()))) {
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append("\n");
            }
            System.out.print("\nBody: " + builder.toString());
        }
    }

}
